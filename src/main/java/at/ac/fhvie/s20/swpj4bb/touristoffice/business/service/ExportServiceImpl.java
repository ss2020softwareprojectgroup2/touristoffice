package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class ExportServiceImpl implements ExportService {

  @Autowired
  private JavaMailSender javaMailSender;

  public File getLastModified(String path) {

    File directory = new File(path);
    File[] files = directory.listFiles(File::isFile);
    long lastModifiedTime = Long.MIN_VALUE;
    File chosenFile = null;

    if (files != null) {
      for (File file : files) {
        if (file.lastModified() > lastModifiedTime) {
          chosenFile = file;
          lastModifiedTime = file.lastModified();
        }
      }
    }
    return chosenFile;
  }

  public void createZip(String pathFullBackup, String pathBackupSchema, String name) throws IOException {

    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
    java.util.Date date = new Date();

    String path = "logs/zip";
    File directory = new File(path);

    // check if the directory exists
    // using the abstract path name
    if (!directory.exists()) {

      try {

        // create directory
        directory.mkdir();

        // display that the directory is created
        System.out.println("Der Ordner existierte nicht und wurde erstellt.");
      } catch (Exception ex) {
        System.out.println("Der Ordner kann nicht erstellt werden: " + ex);
      }
    }

    // out put file
    ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(path + "/" + name + dateFormat.format(date) + ".zip"));

    // create a list to add files to be zipped
    ArrayList<File> files = new ArrayList<>(2);
    files.add(new File(getLastModified(pathFullBackup).getPath()));
    files.add(new File(getLastModified(pathBackupSchema).getPath()));

    // package files
    for (File file : files) {
      //new zip entry and copying inputstream with file to zipOutputStream, after all closing streams
      zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
      FileInputStream fileInputStream = new FileInputStream(file);

      IOUtils.copy(fileInputStream, zipOutputStream);

      fileInputStream.close();
      zipOutputStream.closeEntry();
    }

    zipOutputStream.close();
  }

  public void sendEmail(String path, String receiver, String subject, String mailBody) throws Exception {

    File file = new File(getLastModified(path).getPath());

    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true);
    helper.setTo(receiver);
    FileSystemResource fileSystemResource = new FileSystemResource(new File(getLastModified(path).getPath()));
    helper.setText(readFile(mailBody));
    helper.addAttachment(file.getName(), fileSystemResource);
    helper.setSubject(subject);
    javaMailSender.send(message);
  }

  public String readFile(String file) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(file));
    String line = null;
    StringBuilder stringBuilder = new StringBuilder();
    String lineSeperator = System.getProperty("line.separator");

    try {
      while ((line = reader.readLine()) != null) {
        stringBuilder.append(line);
        stringBuilder.append(lineSeperator);
      }

      return stringBuilder.toString();
    } finally {
      reader.close();
    }
  }
}

