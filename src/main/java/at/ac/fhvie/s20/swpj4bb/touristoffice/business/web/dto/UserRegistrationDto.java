package at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.dto;


import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class UserRegistrationDto {

  private long id;

  private String hotelId;

  @NotEmpty
  private String firstName;

  @NotEmpty
  private String lastName;

  @NotEmpty
  private String password;

  @Email
  @NotEmpty
  private String email;

  @NotEmpty
  private String role;

  private boolean rightToDelete;

  private boolean monthlyPdfSubscription;

  private String action;

}
