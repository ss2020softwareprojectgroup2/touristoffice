package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import java.io.File;
import java.io.IOException;

public interface ExportService {

  File getLastModified(String path);

  void createZip(String pathFullBackup, String pathBackupSchema, String name) throws IOException;

  void sendEmail(String path, String receiver, String subject, String mailBody) throws Exception;

  String readFile(String file) throws IOException;
}
