package at.ac.fhvie.s20.swpj4bb.touristoffice.business.repository;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Occupancy;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface OccupancyRepository extends CrudRepository<Occupancy, Integer> {
    List<Occupancy> findByHotelId(Long hotelId);

    List<Occupancy> findByHotelIdOrderByYearAscMonthAsc(Long hotelId);

    Optional<Occupancy> findById(Long id);
}
