package at.ac.fhvie.s20.swpj4bb.touristoffice.business.validation;


public abstract class ValidationSupport {

  boolean isNullOrEmptyString(final String value) {
    return value == null || value.isEmpty();
  }

  boolean isNullValue(final Object value) {
    return value == null;
  }

  boolean isValueGreaterThanZero(final long value) {
    return value > 0;
  }

  boolean isValueGreaterThanZerofinal(final double value) {
    return value > 0;
  }

  public static boolean isLessEqualZero(final int value) {
    return (value <= 0);
  }

}
