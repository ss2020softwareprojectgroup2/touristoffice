package at.ac.fhvie.s20.swpj4bb.touristoffice.business.validation;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Hotel;
import org.springframework.stereotype.Component;

@Component
public class HotelValidator extends ValidationSupport implements Validator<Hotel> {

  @Override
  public String validate(final Hotel hotel) {
      String error = "";

      if (isNullOrEmptyString(hotel.getName())) {
          error += nameExceptionMessage();
      }

      if (isNullOrEmptyString(hotel.getOwner())) {
          error += ownerExceptionMessage();
      }

      if (isNullOrEmptyString(hotel.getContact())) {
          error += contactExceptionMessage();
      }

      if (isNullOrEmptyString(hotel.getUrl())) {
          error += contactExceptionMessage();
      }

      if (isNullOrEmptyString(hotel.getAddress())) {
          error += addressExceptionMessage();
      }
      if (isNullOrEmptyString(hotel.getCity())) {
          error += cityExceptionMessage();
      }
      if (isNullOrEmptyString(hotel.getCityCode())) {
          error += cityCodeExceptionMessage();
      }
      if (isNullOrEmptyString(hotel.getPhone())) {
          error += phoneExceptionMessage();
      }
      if (isLessEqualZero(hotel.getNoBeds())) {
          error += bedExceptionMessage(hotel.getNoBeds());
      }

      if (isLessEqualZero(hotel.getNoRooms())) {
          error += bedExceptionMessage(hotel.getNoRooms());
      }

      return error;
  }

    private String nameExceptionMessage() {
        return "Name darf nicht leer sein.\n";
    }

    private String ownerExceptionMessage() {
        return "Owner darf nicht leer sein";
    }

    private String contactExceptionMessage() {
        return "Kontakt darf nicht leer sein.\n";
    }

    private String urlExceptionMessage() {
        return "URL darf nicht leer sein.\n";
    }

    private String addressExceptionMessage() {
        return "Adresse darf nicht leer sein.\n";
    }

    private String cityExceptionMessage() {
        return "Stadt darf nicht leer sein.\n";
    }

    private String cityCodeExceptionMessage() {
        return "Postleitzahl darf nicht leer sein.\n";
    }

    private String phoneExceptionMessage() {
        return "Telefon darf nicht leer sein.\n";
    }

    private String bedExceptionMessage(final int errorValue) {
        return "Bettenzahl muss größer als 0 sein (Ist:" + errorValue + ").\n";
    }

    private String roomExceptionMessage(final int errorValue, final int limit) {
        return "Raumzahl muss größer als 0 sein (Ist:" + errorValue + ").\n";
    }

    private String familyFriendlyExceptionMessage() {
        return "Familienfreundlich darf nicht leer sein.\n";
    }

    private String dogFriendlyExceptionMessage() {
        return "Hundefreundlich darf nicht leer sein.\n";
    }

    private String spaExceptionMessage() {
        return "Spa darf nicht leer sein.\n";
    }

    private String fitnessExceptionMessage() {
        return "Fitness darf nicht leer sein.\n";
    }

}
