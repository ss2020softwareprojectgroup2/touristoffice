package at.ac.fhvie.s20.swpj4bb.touristoffice.business.repository;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  Role findByName(String name);
}