package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.User;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;


public interface UserService extends UserDetailsService {

  User findByEmail(String email);

  Optional<User> findById(Long id);

  List<User> findAll();

  User save(UserRegistrationDto registration);

  User update(UserRegistrationDto registration);

  void delete(User user);
}