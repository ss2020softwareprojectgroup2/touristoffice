package at.ac.fhvie.s20.swpj4bb.touristoffice.business.schedulingtasks;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.DatabaseService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.ExportService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.ReportService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.UserService;
import lombok.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Component
@EnableScheduling
public class ScheduledTasks {
  private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

  @Autowired
  final DatabaseService databaseService;
  @Autowired
  final ExportService exportService;
  @Autowired
  final ReportService reportService;
  @Autowired
  final UserService userService;

  @Scheduled(cron = "0 0 12 15 * ?")
  public void createReportAndSendEmail() {
    try {
      reportService.createReportAndSendEmail();
    } catch (Exception ex) {
      log.info("Beim Senden der E-Mail ist ein Fehler aufgetreten: ", dateFormat.format(new Date()), ex);
    }
  }
}