package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.constants.Category;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.converter.CategoryConverter;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Capacity;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Hotel;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.repository.HotelRepository;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.comparator.CapacityComparator;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

@Service
public class HotelService {

  public static final String TOTAL = "Total";
  public static final int GROUP_STAR_MIN = 2;
  private static final String FILENAME_HOTELS = "files" + File.separator + "hotels.txt";
  private static final String COMMA_DELIMITER = ",";
  private static int count = 0;
  private static Set<Hotel> hotels = new HashSet<>();
  private static Map<String, Hotel> hotelsByName = new TreeMap<>();

  private static SortedMap<String, Capacity> hotelsByCapacity =
          new TreeMap<>(new CapacityComparator());

  private HotelRepository hotelRepository;

  @Autowired
  public HotelService(final HotelRepository hotelRepository) {
    this.hotelRepository = hotelRepository;
    readDatabaseFromH2();
  }

  private static void addHotelToCapacity(final Hotel hotel) {
    String index = getStarIndex((hotel.getCategory()));

    Capacity capacity = hotelsByCapacity.get(index);
    Capacity newCapacity =
            new Capacity.CapacityBuilder()
                    .category(index)
                    .businessCount(capacity.getBusinessCount() + 1)
                    .roomCount(capacity.getRoomCount() + hotel.getNoRooms())
                    .bedCount(capacity.getBedCount() + hotel.getNoBeds())
                    .build();
    hotelsByCapacity.put(index, newCapacity);

    // Update total capacity
    Capacity total = hotelsByCapacity.get(TOTAL);
    total.setBusinessCount(total.getBusinessCount() + 1);
    total.setRoomCount(total.getRoomCount() + hotel.getNoRooms());
    total.setBedCount(total.getBedCount() + hotel.getNoBeds());
    hotelsByCapacity.put(TOTAL, total);
  }

  private static void addHotelToHotelNameList(final Hotel hotel) {
    hotelsByName.put(hotel.getName(), hotel);
  }

  private static void clearCapacityMap() {

    hotelsByCapacity.clear();
    for (Category category : Category.values()) {
      Capacity defaultEntity =
              new Capacity.CapacityBuilder()
                      .category(getStarIndex(category))
                      .businessCount(0)
                      .roomCount(0)
                      .bedCount(0)
                      .build();
      hotelsByCapacity.put(getStarIndex(category), defaultEntity);
    }

    Capacity total = new Capacity.CapacityBuilder()
            .category(TOTAL)
            .businessCount(0)
            .bedCount(0)
            .roomCount(0)
            .build();
    hotelsByCapacity.put(TOTAL, total);

  }

  private static String getStarIndex(final Category category) {
    String index;

    if (category.getStarAsNumber() <= GROUP_STAR_MIN) {
      index = Category.TWO + " & " + Category.ONE;
    } else {
      index = category.getStars();
    }

    return index;
  }

  public Hotel findById(final int id) {
    return hotelRepository.findById(id).get();
  }

  public List<Hotel> findAll() {
    return Lists.newArrayList(hotelRepository.findAll());
  }

  private void readDatabaseFromH2() {
    clearCapacityMap();

    for (Hotel hotel : hotelRepository.findAll()) {
      hotels.add(hotel);
      addHotelToCapacity(hotel);
      addHotelToHotelNameList(hotel);
    }

  }

  public boolean save(final Hotel hotel) {
    hotels.add(hotel);
    addHotelToCapacity(hotel);
    addHotelToHotelNameList(hotel);
    hotelRepository.save(hotel);
    exportDatabase();

    return true;
  }

  public void update(final Hotel newHotel) {

    // Data of the original one has to be altered with the new ones.
    Hotel oldHotel = findById(newHotel.getId());
    oldHotel.updateWith(newHotel);
    // update is the same as save! as long as the id is the same!!!!!!!!
    hotelRepository.save(oldHotel);
    exportDatabase();
  }

  public void delete(final Hotel hotelToDelete) {
    hotelRepository.delete(hotelToDelete);
    hotels.remove(hotelToDelete);
  }

  private void exportDatabase() {
    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
    Date date = new Date();

    String sql = StringUtils.EMPTY;
    for (Hotel hotel : hotelRepository.findAll()) {
      sql += "INSERT INTO HOTEL VALUES (";
      sql += "" + hotel.getId() + ", ";
      sql += "" + new CategoryConverter().convertToDatabaseColumn(hotel.getCategory()) + ", ";
      sql += "'" + hotel.getName() + "', ";
      sql += "'" + hotel.getOwner() + "', ";
      sql += "'" + hotel.getContact() + "', ";
      sql += "'" + hotel.getUrl() + "', ";
      sql += "'" + hotel.getAddress() + "', ";
      sql += "'" + hotel.getCity() + "', ";
      sql += "'" + hotel.getCityCode() + "', ";
      sql += "'" + hotel.getPhone() + "', ";
      sql += "" + hotel.getNoRooms() + ", ";
      sql += "" + hotel.getNoBeds() + ", ";
      sql += "" + hotel.isFamilyFriendly() + ", ";
      sql += "" + hotel.isDogFriendly() + ", ";
      sql += "" + hotel.isSpa() + ", ";
      sql += "" + hotel.isFitness() + ");\n";
    }

    BufferedWriter writer = null;
    try {
      String fileName = "logs/hotel" + dateFormat.format(date) + ".sql";
      writer = new BufferedWriter(new FileWriter(fileName));
      writer.write(sql);
      writer.close();

    } catch (IOException exc) {
      exc.printStackTrace();
    }
  }

}
