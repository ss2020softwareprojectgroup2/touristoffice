package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Occupancy;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.repository.OccupancyRepository;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.dto.OccupancyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OccupancyServiceImpl implements OccupancyService {

  @Autowired
  private OccupancyRepository occupancyRepository;

  public List<Occupancy> findByHotelId(Long id) {
    return occupancyRepository.findByHotelId(id);
  }

  public List<Occupancy> findByHotelIdOrderByYearAscMonthAsc(Long id) {
    return occupancyRepository.findByHotelIdOrderByYearAscMonthAsc(id);
  }

  public Optional<Occupancy> findById(Long id) {
    return occupancyRepository.findById(id);
  }

  public void delete(Occupancy occupancy) {
    occupancyRepository.delete(occupancy);
  }

  public void deleteList(List<Occupancy> occupancies) {
    occupancyRepository.deleteAll(occupancies);
  }

  public Occupancy save(OccupancyDto occupancyToSave) {
    Occupancy occupancy = new Occupancy();
    occupancy = setUpOccupancy(occupancy, occupancyToSave);
    return occupancyRepository.save(occupancy);
  }

  public Occupancy update(OccupancyDto occupancyToUpdate) {
    Occupancy occupancy = new Occupancy();
    occupancy.setId(occupancyToUpdate.getId());
    occupancy = setUpOccupancy(occupancy, occupancyToUpdate);
    return occupancyRepository.save(occupancy);
  }

  public Occupancy setUpOccupancy(Occupancy occupancy, OccupancyDto occupancyDto) {
    occupancy.setHotelId(occupancyDto.getHotelId());
    occupancy.setRooms(occupancyDto.getRooms());
    occupancy.setUsedRooms(occupancyDto.getUsedRooms());
    occupancy.setUsedBeds(occupancyDto.getUsedBeds());
    occupancy.setBeds(occupancyDto.getBeds());
    occupancy.setYear(occupancyDto.getYear());
    occupancy.setMonth(occupancyDto.getMonth());
    occupancy.setInternationalGuests(occupancyDto.getInternationalGuests());
    occupancy.setNationality(occupancyDto.getNationality());
    return occupancy;
  }

}

