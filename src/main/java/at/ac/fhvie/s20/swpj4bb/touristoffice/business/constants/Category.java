package at.ac.fhvie.s20.swpj4bb.touristoffice.business.constants;

import java.util.HashMap;
import java.util.Map;

public enum Category {
  FIVE("*****", 5),
  FOUR("****", 4),
  THREE("***", 3),
  TWO("**", 2),
  ONE("*", 1);

  // Reverse-LOOKUP_STARS map for getting a day from an abbreviation
  private static final Map<String, Category> LOOKUP_STARS = new HashMap<>();

  static {
    for (Category category : Category.values()) {
      LOOKUP_STARS.put(category.getStars(), category);
    }
  }

  private String stars;
  private int starAsNumber;
  private int id;

  Category(final String stars, final int starAsNumber) {
    this.stars = stars;
    this.starAsNumber = starAsNumber;
  }

  public static Category get(final String stars) {

    Category category = LOOKUP_STARS.get(stars);
    return category;
  }

  public String getStars() {
    return stars;
  }

  public int getStarAsNumber() {
    return starAsNumber;
  }

  public int getId() {
    return id;
  }

  @Override
  public String toString() {
    return stars;
  }


}
