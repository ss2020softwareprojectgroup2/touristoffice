package at.ac.fhvie.s20.swpj4bb.touristoffice.business.repository;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Hotel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HotelRepository extends CrudRepository<Hotel, Integer> {
  List<Hotel> findByName(String lastName);
}
