package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Occupancy;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.dto.OccupancyDto;

import java.util.List;
import java.util.Optional;


public interface OccupancyService {

  List<Occupancy> findByHotelId(Long id);

  List<Occupancy> findByHotelIdOrderByYearAscMonthAsc(Long id);

  Optional<Occupancy> findById(Long id);

  void delete(Occupancy occupancy);

  void deleteList(List<Occupancy> occupancies);

  Occupancy save(OccupancyDto occupancyDto);

  Occupancy update(OccupancyDto occupancyDto);

}