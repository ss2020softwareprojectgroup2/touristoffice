package at.ac.fhvie.s20.swpj4bb.touristoffice;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Occupancy;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.repository.OccupancyRepository;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.util.Utility;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@SpringBootApplication
public class StartWebApplication {

  public static void main(String[] args) {
    SpringApplication.run(StartWebApplication.class, args);
  }

  @Bean
  CommandLineRunner init(OccupancyRepository occupancyRepository) {
    return args -> {
      List<Occupancy> importData = getImportData();
      occupancyRepository.saveAll(importData);
    };
  }

  private List<Occupancy> getImportData() {
    String filesource = "src/main/resources/files/occupancies.txt";

    ArrayList<String> csvEntries = new ArrayList<>();
    List<Occupancy> occupancyList = new ArrayList<>();

    try (BufferedReader br = new BufferedReader(new FileReader(filesource))) {
      String currentLine;

      while ((currentLine = br.readLine()) != null) {
        csvEntries.add(currentLine);
        csvEntries.toArray();
      }

    } catch (IOException e) {
      e.printStackTrace();
    }

    //remove header
    csvEntries.remove(0);

    String[] countryCodes = Locale.getISOCountries();
    List<String> countryCodesList = new ArrayList<String>(Arrays.asList(countryCodes));
    countryCodesList.remove("AT"); //not international guests :(

    for (String csvEntry : csvEntries) {
      Locale country = new Locale(StringUtils.EMPTY, countryCodesList.get(Utility.getRandom(0, (countryCodesList.size() - 1))));

      Occupancy tempOccupancy = new Occupancy();
      String[] values = csvEntry.split(",");

      tempOccupancy.setHotelId(Long.parseLong(values[0], 10));
      tempOccupancy.setRooms(Integer.parseInt(values[1]));
      tempOccupancy.setUsedRooms(Integer.parseInt(values[2]));
      tempOccupancy.setBeds(Integer.parseInt(values[3]));
      tempOccupancy.setUsedBeds(Integer.parseInt(values[4]));
      tempOccupancy.setYear(Integer.parseInt(values[5]));
      tempOccupancy.setMonth(Integer.parseInt(values[6]));
      tempOccupancy.setInternationalGuests(Utility.getRandom(0, Integer.parseInt(values[1]) * 30));
      tempOccupancy.setNationality(country.getDisplayCountry());

      occupancyList.add(tempOccupancy);
    }

    return occupancyList;
  }
}

