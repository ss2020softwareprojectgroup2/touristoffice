package at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.dto;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.constants.Category;
import lombok.*;

@Data
public class HotelDto {

  private int id;
  private Category category;
  private String name;
  private String owner;
  private String contact;
  private String url;
  private String address;
  private String city;
  private String cityCode;
  private String phone;
  private int noRooms;
  private int noBeds;
  private boolean familyFriendly;
  private boolean dogFriendly;
  private boolean spa;
  private boolean fitness;
  private String action;
}
