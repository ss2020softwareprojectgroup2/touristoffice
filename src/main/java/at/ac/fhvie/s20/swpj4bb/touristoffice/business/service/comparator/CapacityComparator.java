package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.comparator;

import java.util.Comparator;

import static at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.HotelService.TOTAL;

public class CapacityComparator implements Comparator<String> {
  @Override
  public int compare(final String left, final String right) {

    if (left.equals(right)) {
      return 0;
    }

    if (left.equals(TOTAL)) {
      return 1;
    }

    if (right.equals(TOTAL)) {
      return -1;
    }

    if (left.compareTo(right) > 0) {
      return -1;
    }
    if (left.compareTo(right) < 0) {
      return 1;
    }

    return 0;

  }
}
