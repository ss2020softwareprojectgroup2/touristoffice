package at.ac.fhvie.s20.swpj4bb.touristoffice.business.repository;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  User findByEmail(String email);
}
