package at.ac.fhvie.s20.swpj4bb.touristoffice.business.validation;

interface Validator<K> {

  String validate(K value);

}
