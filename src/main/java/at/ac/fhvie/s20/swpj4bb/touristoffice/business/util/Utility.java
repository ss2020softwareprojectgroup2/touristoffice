package at.ac.fhvie.s20.swpj4bb.touristoffice.business.util;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.User;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.ui.Model;

import java.net.URL;
import java.util.Random;

@Data
public final class Utility {

  public static final String REDIRECT_ERROR = "error";
  public static final String REDIRECT_ACCESS = "access";
  private static final String ROLE_HOTEL = "Hotel";
  private static final String ROLE_USER = "User";
  private static final String ROLE_ADMIN = "Admin";


  public static String getProjectDir() {
    try {
      Class<?> callingClass = Class
          .forName(Thread.currentThread().getStackTrace()[2].getClassName());
      URL url = callingClass.getProtectionDomain().getCodeSource().getLocation();
      return url.getPath();
    } catch (ClassNotFoundException exc) {
      exc.printStackTrace();
    }
    return StringUtils.EMPTY;
  }

  public static boolean isNumeric(final String text) {
    String value = text;
    try {
      Integer.parseInt(value);
      return true;
    } catch (NumberFormatException exc) {
      return false;
    }
  }

  public static Model setUpModel(Model model, String view, String message, String action, User actualUser){
    model.addAttribute("view", view);
    model.addAttribute("message", message);
    model.addAttribute("action", action);
    model.addAttribute("actualUser", actualUser);
    return model;
  }

  public static int getRandom(int min, int max) {

    if (min >= max) {
      throw new IllegalArgumentException("max must be greater than min");
    }

    Random r = new Random();
    return r.nextInt((max - min) + 1) + min;
  }

  public static boolean isHotelRole(User actualUser) {
    return ROLE_HOTEL.equals(actualUser.getSpecificRole());
  }

  public static boolean isUserRole(User actualUser) {
    return ROLE_USER.equals(actualUser.getSpecificRole());
  }

  public static boolean isAdminRole(User actualUser) {
    return ROLE_ADMIN.equals(actualUser.getSpecificRole());
  }

}
