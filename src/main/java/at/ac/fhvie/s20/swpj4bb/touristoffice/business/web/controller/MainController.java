package at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.controller;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.User;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.HotelService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.UserService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.util.Utility;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Data
@Controller
public class MainController implements ErrorController {

  private static final String INDEX = "index";
  private static final String REDIRECT = "redirect:/hotel";
  private static final String REDIRECT_DETAIL = "redirect:/occupancy?id=";

  @Autowired
  final HotelService hotelService;

  @Autowired
  final UserService userService;

  @GetMapping("/")
  public String main(Model model, Principal principal) {
    User actualUser = userService.findByEmail(principal.getName());

    if (Utility.isHotelRole(actualUser)) {
      Long hotelId = actualUser.getHotelId();

      if (hotelId == null) {
        Utility.setUpModel(model, Utility.REDIRECT_ACCESS, StringUtils.EMPTY, StringUtils.EMPTY, actualUser);
        return INDEX;
      } else if (hotelId > 0) {
        return REDIRECT_DETAIL + hotelId;
      }

      return Utility.REDIRECT_ERROR;
    }
    return REDIRECT;
  }

  @GetMapping("/login")
  public String login() {
    return "login";
  }

  @RequestMapping("/error")
  public String handleError(Model model, Principal principal) {
    User actualUser = userService.findByEmail(principal.getName());
    Utility.setUpModel(model, Utility.REDIRECT_ERROR, StringUtils.EMPTY, StringUtils.EMPTY, actualUser);
    return INDEX;
  }

  @Override
  public String getErrorPath() {
    return "/error";
  }
}
