package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Hotel;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Occupancy;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.User;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.schedulingtasks.ScheduledTasks;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportServiceImpl implements ReportService {
  private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

  @Autowired
  private OccupancyService occupancyService;
  @Autowired
  private HotelService hotelService;
  @Autowired
  private UserService userService;
  @Autowired
  private ExportService exportService;

  public void exportPdfReport(int id) throws FileNotFoundException, JRException {

    List<Occupancy> occupancyList = occupancyService.findByHotelIdOrderByYearAscMonthAsc((long) id);
    String name = hotelService.findById(id).getName();

    // load and compile file
    File file = ResourceUtils.getFile("classpath:reports/occupancy.jrxml");
    JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());

    // map report to source
    JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(occupancyList);

    // print and fill report
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("Hotel", name);
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

    // export PDF report
    JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\temp\\occupancy.pdf");
  }

  public void createReportAndSendEmail() {
    final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    try {
      List<User> userList = userService.findAll();

      for (User user : userList) {
        if (user.getHotelId() != null && user.isMonthlyPdfSubscription()) {
          exportPdfReport(user.getHotelId().intValue());
          Hotel hotel = hotelService.findById(user.getHotelId().intValue());

          exportService.sendEmail("C:\\temp", user.getEmail(), "Statistik-Report: " + hotel.getName(), "src/main/resources/files/statistik.txt");
          log.info("E-Mail wurde versendet um: ", dateFormat.format(new Date()));
        }
      }
    } catch (Exception ex) {
      log.info("Beim Senden der E-Mail ist ein Fehler aufgetreten: ", dateFormat.format(new Date()), ex);
    }
  }
}
