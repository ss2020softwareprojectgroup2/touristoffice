package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import net.sf.jasperreports.engine.JRException;

import java.io.FileNotFoundException;

public interface ReportService {

  void exportPdfReport(int id) throws FileNotFoundException, JRException;

  void createReportAndSendEmail() throws JRException;

}
