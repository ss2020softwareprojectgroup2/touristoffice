package at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.controller;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.User;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.DatabaseService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.ExportService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.UserService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.util.Utility;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.dto.UserRegistrationDto;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.List;
import java.util.Optional;


@Data
@RequestMapping("/admin")
@Controller
public class AdminController {

  private static final String DEFAULT_VIEW = "admin";
  private static final String REDIRECT = "redirect:/admin";

  @Autowired
  final UserService userService;
  @Autowired
  final DatabaseService databaseService;
  @Autowired
  final ExportService exportService;

  private String message;
  private String action;
  private String view = DEFAULT_VIEW;

  @GetMapping
  public String admin(Model model, Principal principal) {
    List<User> users = userService.findAll();
    User actualUser = userService.findByEmail(principal.getName());

    if (!Utility.isAdminRole(actualUser)) {
      model = Utility.setUpModel(model, Utility.REDIRECT_ACCESS, StringUtils.EMPTY, StringUtils.EMPTY, actualUser);
    } else {
      model = Utility.setUpModel(model, view, message, action, actualUser);
      users.remove(actualUser);
      model.addAttribute("users", users);
    }

    message = StringUtils.EMPTY;
    action = StringUtils.EMPTY;
    return "index";
  }

  @ModelAttribute("user")
  public UserRegistrationDto userRegistrationDto() {
    return new UserRegistrationDto();
  }

  @PostMapping
  public String registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto userDto,
                                    BindingResult result) {
    if (userDto.getAction().equals("UPDATE") && !result.hasErrors()) {
      action = "UPDATE";
      message = "User wurde aktualisiert";
      userService.update(userDto);
      return REDIRECT;
    }
    User existing = userService.findByEmail(userDto.getEmail());
    if (existing != null) {
      result.rejectValue("email", null, "User existiert bereits");
      action = "ERROR";
      message = "User konnte nicht angelegt werden, ein User mit der Email existiert bereits";
    }

    if (!result.hasErrors()) {
      userService.save(userDto);
      action = "ADD";
      message = "User wurde angelegt";
    }

    return REDIRECT;
  }

  @RequestMapping(value = "/delete", method = RequestMethod.GET)
  public String delete(@RequestParam(name = "id") int id) {
    Optional<User> userToDelete = userService.findById((long) id);
    if (userToDelete.get() != null) {
      userService.delete(userToDelete.get());
      message = "User mit der ID " + userToDelete.get().getId() + " wurde gelöscht";
    }
    action = "DELETE";
    return REDIRECT;
  }

  @RequestMapping(value = "/createAndDownloadBackup", method = RequestMethod.GET)
  public StreamingResponseBody getSteamingFile(HttpServletResponse response) throws IOException {

    String pathZip = "logs/zip";

    //creating backups and compressing them into a zip file
    databaseService.backupDatabaseSchema();
    databaseService.backupFullDatabase();
    exportService.createZip("logs/fullBackup", "logs/backupSchema", "backup");

    File file = new File(exportService.getLastModified(pathZip).getPath());
    response.setStatus(HttpServletResponse.SC_OK);
    response.addHeader("Content-Disposition", "attachment; filename= " + file.getName() + "");

    InputStream inputStream = new FileInputStream(new File(exportService.getLastModified(pathZip).getPath()));

    return outputStream -> {
      int nRead;
      byte[] data = new byte[1024];
      while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
        outputStream.write(data, 0, nRead);
      }
      inputStream.close();
    };
  }

  @RequestMapping(value = "/backupPerEmail", method = RequestMethod.GET)
  public String createBackupAndSendEmail() {
    try {

      //creating backups, compressing them into a zip file and sending the zip per email
      databaseService.backupDatabaseSchema();
      databaseService.backupFullDatabase();
      exportService.createZip("logs/fullBackup", "logs/backupSchema", "backup");
      exportService.sendEmail("logs/zip", "fhb180682@fh-vie.ac.at", "Backup NÖ-TO", "src/main/resources/files/backup.txt");

      action = "ADD";
      message = "Email wurde gesendet";
      return REDIRECT;
    } catch (Exception ex) {
      action = "ERROR";
      message = "Beim Senden trat folgender Fehler auf: " + ex;
      return REDIRECT;
    }
  }

  @GetMapping("/findOne")
  @ResponseBody
  public Optional<User> findOne(int id) {
    return userService.findById((long) id);
  }

}