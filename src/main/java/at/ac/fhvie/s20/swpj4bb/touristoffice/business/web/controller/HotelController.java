package at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.controller;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Hotel;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Occupancy;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.User;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.ExportService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.HotelService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.OccupancyService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.ReportService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.UserService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.util.Utility;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.dto.HotelDto;
import lombok.*;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Data
@RequestMapping("/hotel")
@Controller
public class HotelController {

  private static final String DEFAULT_VIEW = "hotel";
  private static final String REDIRECT = "redirect:/hotel";

  @Autowired
  final HotelService hotelService;
  @Autowired
  final UserService userService;
  @Autowired
  final OccupancyService occupancyService;
  @Autowired
  final ReportService reportService;

  final ExportService exportService;

  private String message;
  private String action;
  private String view = DEFAULT_VIEW;

  @GetMapping
  public String hotel(Model model, Principal principal) {
    User actualUser = userService.findByEmail(principal.getName());
    List<Hotel> hotels = hotelService.findAll();
    if (!Utility.isUserRole(actualUser) && !Utility.isAdminRole(actualUser)) {
      model = Utility.setUpModel(model, Utility.REDIRECT_ACCESS, StringUtils.EMPTY, StringUtils.EMPTY, actualUser);
    } else {
      model = Utility.setUpModel(model, view, message, action, actualUser);
      model.addAttribute("hotels", hotels);
    }

    message = "";
    action = "";
    return "index";
  }

  @ModelAttribute("hotel")
  public HotelDto hotelDto() {
    return new HotelDto();
  }

  @PostMapping
  public String save(@ModelAttribute("hotel") HotelDto hDto, BindingResult result) {

    Hotel.HotelBuilder hotelbuilder = new Hotel.HotelBuilder();
    if (hDto.getId() == 0) {
      hotelbuilder = hotelbuilder.id(-1);

      Hotel hotel = hotelbuilder
          .category(hDto.getCategory())
          .name(hDto.getName())
          .owner(hDto.getOwner())
          .contact(hDto.getContact())
          .url(hDto.getUrl())
          .address(hDto.getAddress())
          .city(hDto.getCity())
          .cityCode(hDto.getCityCode())
          .phone(hDto.getPhone())
          .noBeds(hDto.getNoRooms())
          .noRooms(hDto.getNoBeds())
          .familyFriendly(hDto.isFamilyFriendly())
          .dogFriendly(hDto.isDogFriendly())
          .spa(hDto.isSpa())
          .fitness(hDto.isFitness())
          .build();

      hotelService.save(hotel);
      message = "Hotel wurde angelegt";
      action = "ADD";
    } else {

      hotelbuilder = hotelbuilder.id(hDto.getId());

      Hotel hotel = hotelbuilder
          .category(hDto.getCategory())
          .name(hDto.getName())
          .owner(hDto.getOwner())
          .contact(hDto.getContact())
          .url(hDto.getUrl())
          .address(hDto.getAddress())
          .city(hDto.getCity())
          .cityCode(hDto.getCityCode())
          .phone(hDto.getPhone())
          .noBeds(hDto.getNoRooms())
          .noRooms(hDto.getNoBeds())
          .familyFriendly(hDto.isFamilyFriendly())
          .dogFriendly(hDto.isDogFriendly())
          .spa(hDto.isSpa())
          .fitness(hDto.isFitness())
          .build();

      hotelService.update(hotel);
      message = "Hotel wurde verändert";
      action = "UPDATE";
    }

    return REDIRECT;
  }

  @RequestMapping(value = "/delete", method = RequestMethod.GET)
  public String delete(@RequestParam(name = "id") int id) {
    try {
      Hotel hotelToDelete = hotelService.findById(id);
      List<Occupancy> occupancyToDelete = occupancyService.findByHotelId((long) id);
      hotelService.delete(hotelToDelete);
      message = "Hotel mit der ID " + hotelToDelete.getId();
      if (!occupancyToDelete.isEmpty()) {
        occupancyService.deleteList(occupancyToDelete);
        message += " und " + occupancyToDelete.size() + " Belegungsdateneinträgen";

      }
      message += " wurde gelöscht!";
    } catch (Exception ex) {
      message = "Löschen war nicht erfolgreich!";
    }
    action = "DELETE";
    return REDIRECT;
  }

  @RequestMapping(value = "/pdfReport", method = RequestMethod.GET)
  public StreamingResponseBody getSteamingFile(HttpServletResponse response, @RequestParam(name = "id") int id)
      throws IOException, JRException {

    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
    java.util.Date date = new Date();

    //creating the jasperReport
    reportService.exportPdfReport(id);

    String path = "C:\\temp";

    response.setContentType("application/pdf");
    response.setHeader("Content-Disposition", "attachment; filename=occupancy_hotel_" + id + "_" + dateFormat.format(date) + ".pdf");
    InputStream inputStream = new FileInputStream(new File(exportService.getLastModified(path).getPath()));

    return outputStream -> {
      int nRead;
      byte[] data = new byte[1024];
      while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
        outputStream.write(data, 0, nRead);
      }
      inputStream.close();
    };
  }

  @GetMapping("/findOne")
  @ResponseBody
  public Hotel findOne(int id) {
    return hotelService.findById(id);
  }

}

