package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class DatabaseServiceImpl implements DatabaseService {

  private static final String DEFAULT_FULLBACKUP_FOLDER = "logs/fullBackup";
  private static final String DEFAULT_BACKUP_SCHEMA_FOLDER = "logs/backupSchema";

  public void backupFullDatabase() {

    Connection conn = null;
    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
    java.util.Date date = new Date();

    try {
      conn = DriverManager.getConnection("jdbc:h2:~/hotels", "sa", "");
      Statement stmt = conn.createStatement();

      String sql = "SCRIPT TO '" + DEFAULT_FULLBACKUP_FOLDER + "/fullBackup" + dateFormat.format(date) + ".sql'";

      stmt.executeQuery(sql);

    } catch (SQLException exc) {
      exc.printStackTrace();
    }
  }

  public void backupDatabaseSchema() {

    Connection conn = null;
    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
    java.util.Date date = new Date();

    try {
      conn = DriverManager.getConnection("jdbc:h2:~/hotels", "sa", "");
      Statement stmt = conn.createStatement();

      String sql = "SCRIPT NODATA TO '" + DEFAULT_BACKUP_SCHEMA_FOLDER + "/backupSchema" + dateFormat.format(date) + ".sql'";

      stmt.executeQuery(sql);

    } catch (SQLException exc) {
      exc.printStackTrace();
    }
  }
}
