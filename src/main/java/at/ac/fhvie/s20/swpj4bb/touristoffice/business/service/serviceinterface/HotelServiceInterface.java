package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.serviceinterface;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Hotel;

import java.util.Set;

public interface HotelServiceInterface {

  Set<String> findAllHotelNames();

  Hotel findHotelByName(String name);

}
