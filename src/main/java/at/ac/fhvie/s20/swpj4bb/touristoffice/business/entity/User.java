package at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Long hotelId;
  private String firstName;
  private String lastName;
  private String email;
  private String password;
  private boolean rightToDelete;
  private boolean monthlyPdfSubscription;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "users_roles",
      joinColumns = @JoinColumn(
          name = "user_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(
          name = "role_id", referencedColumnName = "id"))
  private Collection<Role> roles;

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", email='" + email + '\'' +
        ", password='" + "*********" + '\'' +
        ", rightToDelete='" + rightToDelete + '\'' +
        ", monthlyPdfSubscription='" + monthlyPdfSubscription + '\'' +
        ", roles=" + roles +
        '}';
  }

  public String getSpecificRole() {
    return this.getRoles().iterator().next().getName();
  }

  public String getFullName() {
    return this.getFirstName().concat(", ".concat(this.getLastName()));
  }
}
