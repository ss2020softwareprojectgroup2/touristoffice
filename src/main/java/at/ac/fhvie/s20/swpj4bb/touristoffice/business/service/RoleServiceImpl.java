package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Role;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class RoleServiceImpl implements RoleService {

  @Autowired
  private RoleRepository roleRepository;

  public Role findByName(String name) {
    return roleRepository.findByName(name);
  }

}
