package at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.controller;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Hotel;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Occupancy;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.User;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.HotelService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.OccupancyService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.service.UserService;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.util.Utility;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.dto.OccupancyDto;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@RequestMapping("/occupancy")
@Controller
public class OccupancyController {

  private static final String DEFAULT_VIEW = "occupancy";
  private static final String REDIRECT = "redirect:/occupancy";
  private static final String REDIRECT_DETAIL = "redirect:/occupancy?id=";

  @Autowired
  final OccupancyService occupancyService;
  @Autowired
  final UserService userService;
  @Autowired
  final HotelService hotelService;

  private String message;
  private String action;

  @GetMapping
  public String occupancy(Model model, Principal principal, int id) {
    User actualUser = userService.findByEmail(principal.getName());
    Hotel actualHotel = hotelService.findById(id);

    model = Utility.setUpModel(model, DEFAULT_VIEW, message, action, actualUser);
    if (Utility.isHotelRole(actualUser) && (actualUser.getHotelId() != id)) {
      model = Utility.setUpModel(model, Utility.REDIRECT_ACCESS, StringUtils.EMPTY, StringUtils.EMPTY, actualUser);
    }

    List<Occupancy> occupancyList = occupancyService.findByHotelIdOrderByYearAscMonthAsc((long) id);

    if (occupancyList != null) {
      List<Integer> years = new ArrayList<>(
          new HashSet<>(
              occupancyList.stream()
                  .map(Occupancy::getYear)
                  .collect(Collectors.toList())
          ));

      Collections.sort(years);
      model.addAttribute("occupancies", occupancyList);
      model.addAttribute("years", years);
    }

    if (actualHotel != null) {
      model.addAttribute("actualHotel", actualHotel);
    }

    message = StringUtils.EMPTY;
    action = StringUtils.EMPTY;
    return "index";
  }

  @ModelAttribute("occupancy")
  public OccupancyDto occupancyDto() {
    return new OccupancyDto();
  }

  @PostMapping
  public String save(@ModelAttribute("occupancy") OccupancyDto occupancyDto, Model model, Principal principal) {

    if (occupancyDto.getId() == 0) {
      occupancyService.save(occupancyDto);
      message = "Occupancy wurde angelegt";
      action = "ADD";
    } else {
      occupancyService.update(occupancyDto);
      message = "Occupany wurde verändert";
      action = "UPDATE";
    }

    return REDIRECT_DETAIL + occupancyDto.getHotelId();
  }

  @GetMapping("/findOne")
  @ResponseBody
  public Optional<Occupancy> findOne(int id) {
    return occupancyService.findById((long) id);
  }

  @RequestMapping(value = "/delete", method = RequestMethod.GET)
  public String delete(@RequestParam(name = "id") int id) {
    Optional<Occupancy> occupancyToDelete = occupancyService.findById((long) id);

    if (occupancyToDelete.get() != null) {
      occupancyService.delete(occupancyToDelete.get());
      message = "Belegungsdaten mit der ID " + occupancyToDelete.get().getId() + " wurde gelöscht!";
      action = "DELETE";
    }

    return REDIRECT_DETAIL + occupancyToDelete.get().getHotelId();
  }
}

