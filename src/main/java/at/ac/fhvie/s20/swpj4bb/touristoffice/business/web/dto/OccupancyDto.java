package at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.dto;

import lombok.*;

@Data
public class OccupancyDto {

  private long id;
  private long hotelId;
  private int rooms;
  private int usedRooms;
  private int beds;
  private int usedBeds;
  private int year;
  private int month;
  private int internationalGuests;
  private String nationality;
  private String action;
}
