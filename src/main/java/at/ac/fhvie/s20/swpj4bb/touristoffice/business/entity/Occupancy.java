package at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public final class Occupancy {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Long hotelId;
  private int rooms;
  private int usedRooms;
  private int beds;
  private int usedBeds;
  private int year;
  private int month;
  private int internationalGuests;
  private String nationality;

}
