package at.ac.fhvie.s20.swpj4bb.touristoffice.business.service;

import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.Role;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.entity.User;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.repository.RoleRepository;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.repository.UserRepository;
import at.ac.fhvie.s20.swpj4bb.touristoffice.business.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private BCryptPasswordEncoder passwordEncoder;

  public User findByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  public Optional<User> findById(Long id) {
    return userRepository.findById(id);
  }

  public User save(UserRegistrationDto userToSave) {
    User user = new User();
    user = setUpUser(user, userToSave);
    return userRepository.save(user);
  }

  public User update(UserRegistrationDto userToUpdate) {
    User user = new User();
    user.setId(userToUpdate.getId());
    user = setUpUser(user, userToUpdate);
    return userRepository.save(user);
  }

  @Override
  public List<User> findAll() {
    return userRepository.findAll();
  }

  public void delete(User user) {
    userRepository.delete(user);
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    User user = userRepository.findByEmail(email);
    if (user == null) {
      throw new UsernameNotFoundException("Invalid username or password.");
    }
    return new org.springframework.security.core.userdetails.User(user.getEmail(),
        user.getPassword(),
        mapRolesToAuthorities(user.getRoles()));
  }

  private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
    return roles.stream()
        .map(role -> new SimpleGrantedAuthority(role.getName()))
        .collect(Collectors.toList());
  }

  public User setUpUser(User user, UserRegistrationDto userRegistrationDto) {
    user.setHotelId(null);

    if (userRegistrationDto.getRole().equals("Hotel")) {
      String hotelId = userRegistrationDto.getHotelId();
      if (hotelId != null && !hotelId.isEmpty()) {
        user.setHotelId(Long.parseLong(hotelId, 10));
      }
    }
    user.setFirstName(userRegistrationDto.getFirstName());
    user.setLastName(userRegistrationDto.getLastName());
    user.setEmail(userRegistrationDto.getEmail());
    user.setPassword(passwordEncoder.encode(userRegistrationDto.getPassword()));
    user.setRightToDelete(userRegistrationDto.isRightToDelete());
    user.setMonthlyPdfSubscription(userRegistrationDto.isMonthlyPdfSubscription());
    user.setRoles(Arrays.asList(roleRepository.findByName(userRegistrationDto.getRole())));
    return user;
  }
}
