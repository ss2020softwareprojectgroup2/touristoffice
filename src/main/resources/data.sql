INSERT INTO HOTEL VALUES (1, 2, 'Zum wilden Eber', 'James Bond', 'Strolchi', 'www.zumwildeneber.at', 'Am Berg 7', 'Großebersdorf', 'A-1007', '0987', 3, 3, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (2, 4, 'Beim Branntweiner', 'Ima Voll', 'Gretel', 'www.branntweiner.at', 'Weinberg 3', 'Alkohütten', 'A-1234', '546456', 1, 1, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (3, 1, 'Drei Sterne und die Sonne', 'Sternwarte', 'Hänsel', 'www.sterneundsonne.at', 'Sterngasse 5', 'Alldorf', 'A-1234', '01234', 40, 4, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (4, 2, 'Zum güldenen Hirsch', 'Harry Hirsch', 'Susi', 'www.zumgueldenenhirsch.at', 'Hirschgasse 42/47/11', 'Hirschhausen', 'A-0815', '1234', 5, 50, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (5, 3, 'The Levante Parliament', 'Il. Literat', 'der böse Wolf', 'www.thelevanteparliament.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-1234', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (6, 3, 'Grand Hotel Wien', 'Il. Literat', 'der böse Wolf', 'www.grandhotel.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-1234', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (7, 3, 'Hotel Bristol', 'Il. Literat', 'der böse Wolf', 'www.bristol.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-1234', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (8, 3, 'Ambassador Hotel', 'Il. Literat', 'der böse Wolf', 'www.ambassador.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-1234', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (9, 3, 'Imperium Residence', 'Il. Literat', 'der böse Wolf', 'www.imperium.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-1234', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (10, 3, 'Palais Coburg Residenz', 'Il. Literat', 'der böse Wolf', 'www.palais.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-1234', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (11, 3, 'Das ABC', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (12, 3, 'as ABC', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (13, 3, 's ABC', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (14, 3, '2ABC', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (15, 3, 'ABC', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (16, 3, 'BC', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (17, 3, 'C', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (18, 3, 'D', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (19, 3, 'Da', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (20, 3, 'Das', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (21, 3, 'Das1', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);
INSERT INTO HOTEL VALUES (22, 3, 'Das A', 'Il. Literat', 'der böse Wolf', 'www.abc.at', 'Heinzelmännchenweg 3', 'Buchstaben', 'A-BCDEF', '9876', 2, 2, 1, 0, 1, 0);

INSERT INTO USER VALUES (1, null, 'stefan@greil.at', 'Stefan', 'Greil' , '$2a$10$xp.obih1BMwnY59zt1qLDecbiHmZb4VnOkfN/Jh1dcVEpzK.yZdiK', 0, 0);
/*==============================================================*/
/* pw = lastname                                                */
/*==============================================================*/
INSERT INTO USER VALUES (2, null, 'admin@noe.at', 'Bob', 'admin' , '$2a$10$qQ/BF2RfV7lVGRSunLMD1eu0nafJj3SbmaScxhoHXlnMjNqidMweq', 0, 0);
INSERT INTO USER VALUES (3, null, 'user@noe.at', 'Bob', 'user1' , '$2a$10$L2HiE0oyY7I8DCzBbX2geuKrrrBHay2pUd4DdytiaUBL1jCH2pxHG', 0, 0);
INSERT INTO USER VALUES (4, null, 'user2@noe.at', 'Bob', 'user2' , '$2a$10$L2HiE0oyY7I8DCzBbX2geuKrrrBHay2pUd4DdytiaUBL1jCH2pxHG', 1, 0);
INSERT INTO USER VALUES (5, 1, 'hotel1@noe.at', 'Bob', 'hotel1' , '$2a$10$FoJvRh.oG/26LBfLS7GrR.f1qwQDPkMJzRQ4OLDJmQCFaqA1d04/e', 0, 0);
INSERT INTO USER VALUES (6, 2, 'hotel2@noe.at', 'Bob', 'hotel2' , '$2a$10$FoJvRh.oG/26LBfLS7GrR.f1qwQDPkMJzRQ4OLDJmQCFaqA1d04/e', 0, 0);

INSERT INTO ROLE VALUES (1, 'User');
INSERT INTO ROLE VALUES (2, 'Admin');
INSERT INTO ROLE VALUES (3, 'Hotel');

INSERT INTO users_roles VALUES (1, 2);
INSERT INTO users_roles VALUES (2, 2);
INSERT INTO users_roles VALUES (3, 1);
INSERT INTO users_roles VALUES (4, 1);
INSERT INTO users_roles VALUES (5, 3);
INSERT INTO users_roles VALUES (6, 3);

INSERT INTO occupancy VALUES(1,1,44,33,100,69,2013,1,217,'Spain');
INSERT INTO occupancy VALUES(2,1,44,30,100,75,2013,2,246,'Australia');
INSERT INTO occupancy VALUES(3,1,44,28,100,54,2013,3,128,'Honduras');
INSERT INTO occupancy VALUES(4,1,44,23,100,57,2013,4,96,'New Zealand');
INSERT INTO occupancy VALUES(5,1,44,28,100,80,2013,5,102,'Spain');
INSERT INTO occupancy VALUES(6,1,50,27,120,60,2013,6,121,'Czechia');
INSERT INTO occupancy VALUES(7,1,50,21,120,75,2013,7,211,'Turkey');
INSERT INTO occupancy VALUES(8,1,50,28,120,100,2013,8,121,'Switzerland');
INSERT INTO occupancy VALUES(9,1,70,58,140,110,2013,9,321,'France');
INSERT INTO occupancy VALUES(10,1,70,38,140,115,2013,10,201,'Switzerland');
INSERT INTO occupancy VALUES(11,1,70,40,140,105,2013,11,210,'Germany');
INSERT INTO occupancy VALUES(12,1,70,50,140,123,2013,12,230,'Switzerland');