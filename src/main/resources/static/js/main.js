// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

$(document).ready(function () {

    $('a.active').removeClass('active');
    $('a[href="' + location.pathname + '"]').closest('a').addClass('active');

    $('#dtBasic').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "lengthChange": false,
        pageLength: 9,
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-right'
            }
        ]
    });
    $('#dtOccupancy').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "lengthChange": false,
        "order": [[1, 'asc'], [2, 'asc']],
        pageLength: 9,
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-right'
            }
        ]
    });
    $('.dataTables_length').addClass('bs-select');

    $('.loader').fadeOut(300, function () {
        $('#view').removeClass('hidden')
    });

    $('.collapse').on('shown.bs.collapse', function (e) {
        var $card = $(this).closest('.card');
        $('html,body').animate({
            scrollTop: $card.offset().top - 80
        }, 400);
    });

    $('.nHotelBtn').on('click', function (event) {
        resetForm('#addHotel');
    });

    $('.eHotelBtn').on('click', function (event) {
        resetForm('#addHotel');
        event.preventDefault();
        var href = $(this).attr('href');
        document.getElementById('addHotel').reset();
        $('#addHotel input[type=checkbox]').removeAttr("checked");
        $.get(href, function (hotel) {
            $('.main #id').val(hotel.id);
            $('.main #name').val(hotel.name);
            $('.main #address').val(hotel.address);
            $('.main #cityCode').val(hotel.cityCode);
            $('.main #city').val(hotel.city);
            $('.main #owner').val(hotel.owner);
            $('.main #phone').val(hotel.phone);
            $('.main #contact').val(hotel.contact);
            $('.main #url').val(hotel.url);
            $('.main #noRooms').val(hotel.noRooms);
            $('.main #noBeds').val(hotel.noBeds);
            $('.main #category').val(hotel.category);
            $('.main #addEditModalCenter').modal();
            if (hotel.familyFriendly) {
                $('.main #familyFriendly').attr("checked", "checked");
            }
            if (hotel.dogFriendly) {
                $('.main #dogFriendly').attr("checked", "checked");
            }
            if (hotel.spa) {
                $('.main #spa').attr("checked", "checked");
            }
            if (hotel.fitness) {
                $('.main #fitness').attr("checked", "checked");
            }
        });
    });

    $('.delHotelBtn').on('click', function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        $('#deleteModalCenter #delRef').attr('href', href);
        $('#deleteModalCenter').modal();
    });


    $('.nOccupancyBtn').on('click', function (event) {
        resetForm('#addOccupancy');
        $('.main #action').val('');
        $('.main #id').val('0');
        var queryString = window.location.search;
        var urlParams = new URLSearchParams(queryString);
        $('.main #hotelId').val(urlParams.get('id'));
    });

    $('.eOccupancyBtn').on('click', function (event) {
        resetForm('#addOccupancy');
        event.preventDefault();
        var href = $(this).attr('href');
        document.getElementById('addOccupancy').reset();
        $.get(href, function (occupancy) {
            $('.main #id').val(occupancy.id);
            $('.main #hotelId').val(occupancy.hotelId);
            $('.main #rooms').val(occupancy.rooms);
            $('.main #usedRooms').val(occupancy.usedRooms);
            $('.main #beds').val(occupancy.beds);
            $('.main #usedBeds').val(occupancy.usedBeds);
            $('.main #year').val(occupancy.year);
            $('.main #month').val(occupancy.month);
            $('.main #internationalGuests').val(occupancy.internationalGuests);
            $('.main #nationality').val(occupancy.nationality);
            $('.main #addEditModalCenter').modal();
            $('.main #action').val('UPDATE');
        });
    });

    $('.delOccupancyBtn').on('click', function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        $('#deleteModalCenter #delRef').attr('href', href);
        $('#deleteModalCenter').modal();
    });

    $('.delAdminBtn').on('click', function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        $('#deleteModalCenter #delRef').attr('href', href);
        $('#deleteModalCenter').modal();
    });

    $('.eAdminBtn').on('click', function (event) {
        resetForm('#addUser');
        event.preventDefault();
        var href = $(this).attr('href');
        document.getElementById('addUser').reset();
        $('#addUser input[type=checkbox]').removeAttr("checked"); // reset checkboxes
        $('#rightToDeleteFormGroup').hide();
        $('#hotelIdFormGroup').hide();
        $('#monthlyPdfSubscriptionFormGroup').hide();

        $.get(href, function (user) {
            $('.main #id').val(user.id);
            $('.main #firstName').val(user.firstName);
            $('.main #lastName').val(user.lastName);
            $('.main #email').val(user.email);
            $('.main #action').val('UPDATE');
            $('.main #role').val(user.roles[0].name);
            $('.main #addEditModalCenter').modal();

            if (user.rightToDelete) {
                $('.main #rightToDelete').attr("checked", "checked");
            }

            if (user.monthlyPdfSubscription) {
                $('.main #monthlyPdfSubscription').attr("checked", "checked");
            }

            if (user.roles[0].name === 'User') {
                $('#rightToDeleteFormGroup').show();
            }
            if (user.roles[0].name === 'Hotel') {
                $('#hotelIdFormGroup').show();
                $('.main #hotelId').val(user.hotelId);
                $('#monthlyPdfSubscriptionFormGroup').show();
                $('.main #hotelId').attr("required", true);
            }
        });
    });

    $('.nAdminBtn').on('click', function (event) {
        resetForm('#addUser');
        $('#addUser input[type=checkbox]').removeAttr("checked"); // reset checkboxes
        $('#rightToDeleteFormGroup').hide();
        $('#hotelIdFormGroup').hide();
        $('#monthlyPdfSubscriptionFormGroup').hide();
        $('.main #action').val('');
        $('.main #id').val('0');
    });

    $('.main #role').change(function () {
        if ($(this).val() === 'User') {
            $('#rightToDeleteFormGroup').show();
        } else {
            $('#rightToDeleteFormGroup').hide();
            $('.main #rightToDelete').removeAttr("checked");
            $('.main #rightToDelete').val(false);
        }
        if ($(this).val() === 'Hotel') {
            $('#hotelIdFormGroup').show();
            $('#monthlyPdfSubscriptionFormGroup').show();
            $('.main #hotelId').attr("required", true);
        } else {
            $('#hotelIdFormGroup').hide();
            $('.main #hotelId').val('');
            $('#monthlyPdfSubscriptionFormGroup').hide();
            $('.main #monthlyPdfSubscription').removeAttr("checked");
            $('.main #monthlyPdfSubscription').val(false);
            $('.main #hotelId').attr("required", false);
        }
    });
});

function createBar(name, ids, data) {
    return new Chartist.Bar(name, {
        labels: ids,
        series: [data]
    }, {
        seriesBarDistance: 10,
        axisX: {
            offset: 60
        },
        axisY: {
            offset: 20,
            labelInterpolationFnc: function (value) {
                return value;
            },
            scaleMinSpace: 15
        },
        width: 400,
        height: 250
    });
}

function createPie(name, data) {

    var labels = data.map(function (data) {
        return data.id
    });
    var count = data.map(function (data) {
        return data.count
    });

    var data = {
        labels: labels,
        series: count
    };

    var options = {
        labelInterpolationFnc: function (value) {
            return value[0]
        }
    };

    var responsiveOptions = [
        ['screen and (min-width: 640px)', {
            chartPadding: 150,
            labelOffset: 20,
            labelDirection: 'explode',
            labelInterpolationFnc: function (value) {
                return value;
            }
        }],
        ['screen and (min-width: 1024px)', {
            labelOffset: 70,
            chartPadding: 10
        }]
    ];

    return new Chartist.Pie(name, data, options, responsiveOptions);
}

function displayError(id) {
    var hideElement = document.getElementById("collapseBody_" + id);
    hideElement.classList.add("hidden");
    var showElement = document.getElementById("hiddenCollapseBody_" + id);
    showElement.classList.remove("hidden");
}

function generateStats(year) {

    if (typeof occupancies !== 'undefined' && occupancies.length > 0) {
        var ids = [];
        var month = [];
        var rooms = [];
        var beds = [];
        var usedRooms = [];
        var usedBeds = [];
        var intGuests = [];
        var yearOccupancies = [];
        var nationality = [];

        //sort year
        for (var i in occupancies) {
            if (occupancies[i].year === year) {
                yearOccupancies.push(occupancies[i]);
            }
        }

        //sort by month
        yearOrderedOccupancies = _.sortBy(yearOccupancies, ['month']);

        if (yearOccupancies.length > 12) {
            console.error('more than 12 occupancies');
            displayError(year);
            return;
        }

        if (yearOccupancies.length < 12) {
            console.error('less than 12 occupancies');
            displayError(year);
            return;
        }

        if (yearOccupancies.length === 12) {
            for (var i in yearOrderedOccupancies) {
                ids.push(yearOccupancies[i].id);
                month.push(yearOccupancies[i].month);
                rooms.push(yearOccupancies[i].rooms);
                beds.push(yearOccupancies[i].beds);
                usedRooms.push(yearOccupancies[i].usedRooms);
                usedBeds.push(yearOccupancies[i].usedBeds);
                intGuests.push(yearOccupancies[i].internationalGuests);
                nationality.push(yearOccupancies[i].nationality);
            }

            createBar('#chart-rooms_' + year, month, rooms);
            createBar('#chart-usedRooms_' + year, month, usedRooms);
            createBar('#chart-beds_' + year, month, beds);
            createBar('#chart-usedBeds_' + year, month, usedBeds);
            createBar('#chart-intGuests_' + year, month, intGuests);

            var distictNationality = Array.from(new Set(nationality));
            var factor = 100 / distictNationality.length;

            var result = _.map(distictNationality, function (distictNationality) {
                var length = _.reject(nationality, function (el) {
                    return (el.indexOf(distictNationality) < 0);
                }).length;
                return {id: distictNationality, count: (length * factor)};
            });

            createPie('#chart-nat_' + year, result);
        } else {
            console.error('generateStats failed');
            displayError(year);
            return;
        }
    }
}

function resetForm(formName) {
    var form = $(formName)[0];
    $(form).removeClass('was-validated');
    form.reset();
}

window.setTimeout(function () {
    $(".alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
    });
}, 5000);