/*==============================================================*/

DROP TABLE IF EXISTS hotel;

/*==============================================================*/
/* Table: hotel                                                 */
/*==============================================================*/
CREATE TABLE hotel
(
  id      INT  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  category int NOT NULL,
  name    TEXT NOT NULL,
  owner   TEXT NOT NULL,
  contact TEXT NOT NULL,
  url     TEXT NOT NULL,
  address TEXT NOT NULL,
  city    TEXT NOT NULL,
  zip     TEXT NOT NULL,
  phone   TEXT NOT NULL,
  rooms   int  not null,
  beds    int  not null,
  family_Friendly BIT NOT NULL,
  dog_Friendly BIT NOT NULL,
  spa BIT NOT NULL,
  fitness BIT NOT NULL
);
/*==============================================================*/
DROP TABLE IF EXISTS role;

CREATE TABLE role
(
    id   BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name TEXT   NOT NULL
);


/*==============================================================*/
DROP TABLE IF EXISTS user;

CREATE TABLE user
(
    id         BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    hotel_id   BIGINT DEFAULT NULL,
    email      TEXT DEFAULT NULL,
    first_name TEXT DEFAULT NULL,
    last_name  TEXT DEFAULT NULL,
    password   TEXT DEFAULT NULL,
    right_to_delete BIT NOT NULL,
    monthly_pdf_subscription BIT NOT NULL
);

/*==============================================================*/
DROP TABLE IF EXISTS users_roles;

CREATE TABLE users_roles
(
    user_id BIGINT NOT NULL,
    role_id BIGINT NOT NULL
);

DROP TABLE IF EXISTS occupancy;

/*==============================================================*/
/* Table: occupancy id,rooms,usedrooms,beds,usedbeds,internationalguests,nationality,year,month                                         */
/*==============================================================*/
CREATE TABLE occupancy
(
    id                   BIGINT  NOT NULL PRIMARY KEY AUTO_INCREMENT,
    hotel_id             BIGINT  not null,
    rooms                int     not null,
    used_rooms           int     not null,
    beds                 int     not null,
    used_beds            int     not null,
    year                 int     not null,
    month                int     not null,
    international_guests int     not null,
    nationality          varchar not null
);
